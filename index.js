"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

exports.encodeOpts = opts => {
  let str = "";
  for (let key in opts) {
    if (str !== "") {
      str += "&";
    }
    str += key + "=" + encodeURIComponent(JSON.stringify(opts[key]));
  }
  return str;
};
